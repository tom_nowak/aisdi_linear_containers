/*
    Tomasz Nowak
    nr indeksu: 277117
    piątek nieparzysty, godzina 8
*/


#include <cstddef>
#include <cstdlib>
#include <string>
#include <iostream>
#include <complex>

#include "Vector.h"
#include "LinkedList.h"
#include "testVector.h"
#include "testLinkedList.h"


#include "testPerformance.h"

namespace
{
    Performance performance_tester;

    template <typename T>
    using LinearCollection = aisdi::LinkedList<T>; // can be changed to Vector

    #define TEST(xx) \
    { \
        LinearCollection<unsigned> collection; \
        performance_tester.start("append all(" #xx " <unsigned>)"); \
        for(unsigned i=0; i<xx; ++i) \
            collection.append(i); \
        performance_tester.stop("append all(" #xx " <unsigned>)"); \
        performance_tester.start("destructor(" #xx " <unsigned>)"); \
    } \
    performance_tester.stop("destructor(" #xx " <unsigned>)"); \
    { \
        LinearCollection<unsigned> collection; \
        performance_tester.start("prepend all(" #xx " <unsigned>)"); \
        for(unsigned i=0; i<xx; ++i) \
            collection.prepend(i); \
        performance_tester.stop("prepend all(" #xx " <unsigned>)"); \
        performance_tester.start("iterate for all(" #xx " <unsigned>)"); \
        for(auto &i : collection) \
            i*=iterations; \
        performance_tester.stop("iterate for all(" #xx " <unsigned>)"); \
        performance_tester.start("getSize(" #xx " <unsigned>)"); \
        auto size = collection.getSize(); \
        performance_tester.stop("getSize(" #xx " <unsigned>)"); \
        performance_tester.start("get random iterator(" #xx " <unsigned>)"); \
        auto it = collection.cbegin()+iterations%size; \
        performance_tester.stop("get random iterator(" #xx " <unsigned>)"); \
        performance_tester.start("insert random(" #xx " <unsigned>)"); \
        collection.insert(it, iterations); \
        performance_tester.stop("insert random(" #xx " <unsigned>)"); \
        it = collection.cend() - (size+10111*iterations)%size - 1; \
        performance_tester.start("erase random(" #xx " <unsigned>)"); \
        collection.erase(it); \
        performance_tester.stop("erase random(" #xx " <unsigned>)"); \
    }

    void performanceTest()
    {
        for(unsigned iterations=0; iterations<50; ++iterations)
        {
            TEST(10);
            TEST(100);
            TEST(1000);
            TEST(10000);
        }
    }

    #undef TEST
} // namespace


int main(int argc, char** argv)
{
    (void)argv;
    try
    {
        if(argc>1)
        {
           performanceTest();
           performance_tester.end();
        }
        else // tests like the ones using Boost
        {
            test_vector::perform_vector_test();
            test_linked_list::perform_list_test();
        }
        return 0;
    }
    catch(const std::exception &ex) {std::cerr << "Exception: " << ex.what() << "\n"; }
    catch(...) {std::cerr << "Unknown exception.\n"; }
}
