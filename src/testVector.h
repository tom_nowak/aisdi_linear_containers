#ifndef AISDI_LINEAR_SIMPLE_TEST_VECTOR_H
#define AISDI_LINEAR_SIMPLE_TEST_VECTOR_H

#include <iostream>
#include <stdexcept>
#include <cstddef>
#include <initializer_list>
#include "Vector.h"

// Code produced out of VectorTests.cpp, mostly with macros and search-replace,
// so please do not expect it to be easily - readable.
// The reason why I did it is the fact that Boost tests are bugged and they generate memory leaks.
// This test allows to check if there are memory leaks in the container itself.
// Testing function in this file performs the same operations in a simple way
// (without various template specialization, tests at runtime, not compile time).

namespace test_vector
{
    using LinearCollection = aisdi::Vector<int>;

    void thenCollectionContainsValues(const LinearCollection& collection,
                                      const std::initializer_list<int> &expected)
    {
        auto it_c = collection.cbegin();
        auto it_e = expected.begin();
        for(;;)
        {
            if(*it_c != *it_e)
                throw("Failed check: thenCollectionContainsValues");
            if(++it_c==collection.end())
            {
                if(++it_e==expected.end())
                {
                    ;
                    return;
                }
                throw("Failed check: thenCollectionContainsValues");
            }
            else
                if(++it_e==expected.end())
                    throw("Failed check: thenCollectionContainsValues");
        }
    }

    LinearCollection::iterator end(const LinearCollection& collection)
    {
        return collection.end();
    }

    LinearCollection::iterator begin(const LinearCollection& collection)
    {
        return collection.begin();
    }

    void perform_vector_test()
    {
	unsigned number_of_tests = 0;
	std::cout<<"Performing vector tests...\n";
        try
        {
            ++number_of_tests;
            // test_GivenCollection_WhenCreatedWithDefaultConstructor_ThenItIsEmpty()
            {
                const LinearCollection collection;

                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenAddingItem_ThenItIsNoLongerEmpty()
            {
                LinearCollection collection;

                collection.append(1);

                if(!collection.isEmpty()) ; else throw("Failed check (" "!collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenGettingIterators_ThenBeginEqualsEnd()
            {
                LinearCollection collection;

                if(begin(collection) == end(collection)) ; else throw("Failed check (" "begin(collection) == end(collection)" ")");
                if(const_cast<const LinearCollection&>(collection).begin() == collection.end()) ; else throw("Failed check (" "const_cast<const LinearCollection&>(collection).begin() == collection.end()" ")");
                if(collection.cbegin() == collection.cend()) ; else throw("Failed check (" "collection.cbegin() == collection.cend()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenGettingIterator_ThenBeginIsNotEnd()
            {
                LinearCollection collection;
                collection.append(2);

                if(collection.begin() != collection.end()) ; else throw("Failed check (" "collection.begin() != collection.end()" ")");
            }

            ++number_of_tests;
            // test_GivenCollectionWithOneElement_WhenIterating_ThenElementIsReturned()
            {
                LinearCollection collection;
                collection.append(753);

                auto it = collection.begin();

                if(*it==753) ; else throw("Failed check equal (" "*it" "==" "753"")");
                if(++it == collection.end()) ; else throw("Failed check (" "++it == collection.end()" ")");
            }

            ++number_of_tests;
            // test_GivenIterator_WhenPostIncrementing_ThenPreviousPositionIsReturned()
            {
                LinearCollection collection;
                collection.append(1);

                auto it = collection.begin();
                auto postIncrementedIt = it++;

                if(postIncrementedIt == collection.begin()) ; else throw("Failed check (" "postIncrementedIt == collection.begin()" ")");
                if(it == collection.end()) ; else throw("Failed check (" "it == collection.end()" ")");
                if(postIncrementedIt == collection.cbegin()) ; else throw("Failed check (" "postIncrementedIt == collection.cbegin()" ")");
                if(it == collection.cend()) ; else throw("Failed check (" "it == collection.cend()" ")");
            }

            ++number_of_tests;
            // test_GivenIterator_WhenPreIncrementing_ThenNewPositionIsReturned()
            {
                LinearCollection collection;
                collection.append(1);

                auto it = collection.begin();
                auto preIncrementedIt = ++it;

                if(preIncrementedIt == it) ; else throw("Failed check (" "preIncrementedIt == it" ")");
                if(it == collection.end()) ; else throw("Failed check (" "it == collection.end()" ")");
            }

            ++number_of_tests;
            // test_GivenEndIterator_WhenIncrementing_ThenOperationThrows()
            {
                LinearCollection collection;

                try { collection.end()++; throw("Failed check throw (" "collection.end()++" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; }
                try { ++(collection.end()); throw("Failed check throw (" "++(collection.end())" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; }
                try { collection.cend()++; throw("Failed check throw (" "collection.cend()++" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; }
                try { ++(collection.cend()); throw("Failed check throw (" "++(collection.cend())" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; }
            }

            ++number_of_tests;
            // test_GivenEndIterator_WhenDecrementing_ThenIteratorPointsToLastItem()
            {
                LinearCollection collection;
                collection.append(1);
                collection.append(2);

                auto it = collection.end();
                --it;

                if(*it==2) ; else throw("Failed check equal (" "*it" "==" "2"")");
            }

            ++number_of_tests;
            // test_GivenIterator_WhenPreDecrementing_ThenNewIteratorValueIsReturned()
            {
                LinearCollection collection;
                collection.append(1);

                auto it = collection.end();
                auto preDecremented = --it;

                if(it == preDecremented) ; else throw("Failed check (" "it == preDecremented" ")");
                if(*it==1) ; else throw("Failed check equal (" "*it" "==" "1"")");
            }

            ++number_of_tests;
            // test_GivenIterator_WhenPostDecrementing_ThenOldIteratorValueIsReturned()
            {
                LinearCollection collection;
                collection.append(1);

                auto it = collection.end();
                auto postDecremented = it--;

                if(postDecremented == collection.end()) ; else throw("Failed check (" "postDecremented == collection.end()" ")");
                if(*it==1) ; else throw("Failed check equal (" "*it" "==" "1"")");
            }

            ++number_of_tests;
            // test_GivenBeginIterator_WhenDecrementing_ThenOperationThrows()
            {
                LinearCollection collection;

                try { collection.begin()--; throw("Failed check throw (" "collection.begin()--" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
                try { --(collection.begin()); throw("Failed check throw (" "--(collection.begin())" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
                try { collection.cbegin()--; throw("Failed check throw (" "collection.cbegin()--" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
                try { --(collection.cbegin()); throw("Failed check throw (" "--(collection.cbegin())" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
            }

            ++number_of_tests;
            // test_GivenEndIterator_WhenDereferencing_ThenOperationThrows()
            {
                LinearCollection collection;

                try { *collection.end(); throw("Failed check throw (" "*collection.end()" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
                try { *collection.cend(); throw("Failed check throw (" "*collection.cend()" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
            }

            ++number_of_tests;
            // test_GivenConstIterator_WhenDereferencing_ThenItemIsReturned()
            {
                LinearCollection collection = { 10, 20, 30 };

                auto it = ++collection.cbegin();

                if(*it==20) ; else throw("Failed check equal (" "*it" "==" "20"")");
            }

            ++number_of_tests;
            // test_GivenIterator_WhenDereferencing_ThenItemCanBeChanged()
            {
                LinearCollection collection = { 10, 20, 30 };

                auto it = ++begin(collection);
                *it = 500;

                thenCollectionContainsValues(collection, { 10, 500, 30 });
            }

            ++number_of_tests;
            // test_GivenIterator_WhenAddingInteger_ThenAdvancedIteratorIsReturned()
            {
                LinearCollection collection = { 2001, 2010, 2051 };

                auto it = begin(collection);

                if(it + 3 == end(collection)) ; else throw("Failed check (" "it + 3 == end(collection)" ")");
            }

            ++number_of_tests;
            // test_GivenIterator_WhenSubstractingInteger_ThenChangedIteratorIsReturned()
            {
                LinearCollection collection = { 2001, 2010, 2051 };

                auto it = end(collection);

                if(it - 2 == ++begin(collection)) ; else throw("Failed check (" "it - 2 == ++begin(collection)" ")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenAddingItem_ThenItemIsInCollection()
            {
                LinearCollection collection;

                collection.append(42);

                thenCollectionContainsValues(collection, { 42 });
            }

            ++number_of_tests;
            // test_GivenCollection_WhenInitializingFromList_ThenAllItemsAreInCollection()
            {
                const LinearCollection collection = { 1410, 753, 1789 };

                thenCollectionContainsValues(collection, { 1410, 753, 1789 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenCreatingCopy_ThenAllItemsAreCopied()
            {
                LinearCollection collection = { 1410, 753, 1789 };
                LinearCollection other{collection};

                collection.append(1024);

                thenCollectionContainsValues(collection, { 1410, 753, 1789, 1024 });
                thenCollectionContainsValues(other, { 1410, 753, 1789 });
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenCreatingCopy_ThenBothCollectionsAreEmpty()
            {
                LinearCollection collection;
                LinearCollection other{collection};

                if(other.isEmpty()) ; else throw("Failed check (" "other.isEmpty()" ")");
                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenMovingToOther_ThenAllItemsAreMoved()
            {
                LinearCollection collection = { 1410, 753, 1789 };
                LinearCollection other{std::move(collection)};

                thenCollectionContainsValues(other, { 1410, 753, 1789 });
                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenMovingToOther_ThenBothCollectionsAreEmpty()
            {
                LinearCollection collection;
                LinearCollection other{std::move(collection)};

                if(other.isEmpty()) ; else throw("Failed check (" "other.isEmpty()" ")");
                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenAssigningToOther_ThenAllElementsAreCopied()
            {
                const LinearCollection collection = { 1, 2, 3, 4 };
                LinearCollection other = { 100, 200, 300, 400 };

                other = collection;

                thenCollectionContainsValues(other, { 1, 2, 3, 4 });
                thenCollectionContainsValues(collection, { 1, 2, 3, 4 });
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenAssigningToOther_ThenOtherCollectionIsEmpty()
            {
                const LinearCollection collection;
                LinearCollection other = { 100, 200, 300, 400 };

                other = collection;

                if(other.isEmpty()) ; else throw("Failed check (" "other.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenMoveAssigning_ThenAllElementsAreMoved()
            {
                LinearCollection collection = { 1, 2, 3, 4 };
                LinearCollection other = { 100, 200, 300, 400 };

                other = std::move(collection);

                thenCollectionContainsValues(other, { 1, 2, 3, 4 });
                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenMoveAssigning_ThenBothCollectionAreEmpty()
            {
                LinearCollection collection;
                LinearCollection other = { 100, 200, 300, 400 };

                other = std::move(collection);

                if(other.isEmpty()) ; else throw("Failed check (" "other.isEmpty()" ")");
                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenCollection_WhenAppendingItem_ThenItemIsLast()
            {
                LinearCollection collection = { 1, 2, 3 };

                collection.append(42);

                thenCollectionContainsValues(collection, { 1, 2, 3, 42 });
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenPrependingItem_ThenItemIsAdded()
            {
                LinearCollection collection;

                collection.prepend(300);

                thenCollectionContainsValues(collection, { 300 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPrependingItem_ThenItemIsFirst()
            {
                LinearCollection collection = { 1, 2 };

                collection.prepend(300);

                thenCollectionContainsValues(collection, { 300, 1, 2 });
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenGettingSize_ThenZeroIsReturned()
            {
                const LinearCollection collection;

                if(collection.getSize()==0) ; else throw("Failed check equal (" "collection.getSize()" "==" "0"")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenGettingSize_ThenElementCountIsReturned()
            {
                const LinearCollection collection = { 12, 100, 500 };

                if(collection.getSize()==3) ; else throw("Failed check equal (" "collection.getSize()" "==" "3"")");
            }

            ++number_of_tests;
            // test_GivenCollection_WhenChangingIt_ThenItsSizeAlsoChanges()
            {
                LinearCollection collection = { 72, 27, 77 };
                collection.append(99);

                if(collection.getSize()==4) ; else throw("Failed check equal (" "collection.getSize()" "==" "4"")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPrependingItem_ThenSizeIsUpdated()
            {
                LinearCollection collection = { 72, 27, 77 };
                collection.prepend(99);

                if(collection.getSize()==4) ; else throw("Failed check equal (" "collection.getSize()" "==" "4"")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenInsertingItem_ThenItemIsAdded()
            {
                LinearCollection collection;

                collection.insert(begin(collection), 42);

                thenCollectionContainsValues(collection, { 42 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenInsertingAtBegin_ThenItemIsPrepended()
            {
                LinearCollection collection = { 11, 12, 13 };

                collection.insert(begin(collection), 42);

                thenCollectionContainsValues(collection, { 42, 11, 12, 13 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenInsertingAtEnd_ThenItemIsAppended()
            {
                LinearCollection collection = { 11, 12, 13 };

                collection.insert(end(collection), 42);

                thenCollectionContainsValues(collection, { 11, 12, 13, 42 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenInsertingInMiddle_ThenItemInserted()
            {
                LinearCollection collection = { 11, 12, 13 };

                collection.insert(++begin(collection), 42);

                thenCollectionContainsValues(collection, { 11, 42, 12, 13 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenInserting_ThenSizeIsUpdated()
            {
                LinearCollection collection = { 101, 102, 103 };

                collection.insert(begin(collection), 27);

                if(collection.getSize()==4) ; else throw("Failed check equal (" "collection.getSize()" "==" "4"")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenPoppingFirst_ThenOperationThrows()
            {
                LinearCollection collection;

                try { collection.popFirst(); throw("Failed check throw (" "collection.popFirst()" ", " "std::logic_error" ")"); } catch(const std::logic_error &) { ; };
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenPoppingLast_ThenOperationThrows()
            {
                LinearCollection collection;

                try { collection.popLast(); throw("Failed check throw (" "collection.popLast()" ", " "std::logic_error" ")"); } catch(const std::logic_error &) { ; };
            }

            ++number_of_tests;
            // test_GivenCollectionWithSingleItem_WhenPoppingFirst_ThenCollectionIsEmpty()
            {
                LinearCollection collection = { 420 };

                collection.popFirst();

                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenCollectionWithSingleItem_WhenPoppingLast_ThenCollectionIsEmpty()
            {
                LinearCollection collection = { 420 };

                collection.popLast();

                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPoppingFirst_ThenCollectionSizeIsReduced()
            {
                LinearCollection collection = { 14, 10 };

                collection.popFirst();

                if(collection.getSize()==1) ; else throw("Failed check equal (" "collection.getSize()" "==" "1"")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPoppingLast_ThenCollectionSizeIsReduced()
            {
                LinearCollection collection = { 14, 10 };

                collection.popLast();

                if(collection.getSize()==1) ; else throw("Failed check equal (" "collection.getSize()" "==" "1"")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPoppingFirst_ThenItemIsRemoved()
            {
                LinearCollection collection = { 300, 8, 480 };

                collection.popFirst();

                thenCollectionContainsValues(collection, { 8, 480 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPoppingLast_ThenItemIsRemoved()
            {
                LinearCollection collection = { 300, 8, 480 };

                collection.popLast();

                thenCollectionContainsValues(collection, { 300, 8 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPoppingFirst_ThenItemsIsReturned()
            {
                LinearCollection collection = { 101, 202, 303 };

                if(collection.popFirst()==101) ; else throw("Failed check equal (" "collection.popFirst()" "==" "101"")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPoppingLast_ThenItemsIsReturned()
            {
                LinearCollection collection = { 101, 202, 303 };

                if(collection.popLast()==303) ; else throw("Failed check equal (" "collection.popLast()" "==" "303"")");
            }

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenErasing_ThenOperationThrows()
            {
                LinearCollection collection;

                try { collection.erase(collection.begin()); throw("Failed check throw (" "collection.erase(collection.begin())" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingEnd_ThenOperationThrows()
            {
                LinearCollection collection = { 20, 16 };

                try { collection.erase(end(collection)); throw("Failed check throw (" "collection.erase(end(collection))" ", " "std::out_of_range" ")"); } catch(const std::out_of_range &) { ; };
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingBegin_ThenItemIsRemoved()
            {
                LinearCollection collection = { 22, 41, 31 };

                collection.erase(begin(collection));

                thenCollectionContainsValues(collection, { 41, 31 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingLastItem_ThemItemIsRemoved()
            {
                LinearCollection collection = { 22, 45, 33 };

                collection.erase(--end(collection));

                thenCollectionContainsValues(collection, { 22, 45 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingMiddleItem_ThenItemIsRemoved()
            {
                LinearCollection collection = { 22, 51, 48 };

                collection.erase(++begin(collection));

                thenCollectionContainsValues(collection, { 22, 48 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasing_ThenSizeIsReduced()
            {
                LinearCollection collection = { 1000, 500, 2, 900 };

                collection.erase(begin(collection) + 2);

                if(collection.getSize()==3) ; else throw("Failed check equal (" "collection.getSize()" "==" "3"")");
            }

            ++number_of_tests;
            // test_GivenCollectionWithSingleItem_WhenErasing_ThenCollectionIsEmpty()
            {
                LinearCollection collection = { 1529 };

                collection.erase(begin(collection));

                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingEmptyRange_ThenNothingHappens()
            {
                LinearCollection collection = { 19, 42, 11 };

                collection.erase(begin(collection), begin(collection));

                thenCollectionContainsValues(collection, { 19, 42, 11 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingRangeFromBegin_ThenItemsAreRemoved()
            {
                LinearCollection collection = { 19, 42, 11 };

                collection.erase(begin(collection), begin(collection) + 2);

                thenCollectionContainsValues(collection, { 11 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_whenErasingRangeToEnd_ThenItemsAreRemoved()
            {
                LinearCollection collection = { 20, 1, 45 };

                collection.erase(begin(collection) + 1, end(collection));

                thenCollectionContainsValues(collection, { 20 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingSingleItemRange_ThenItemIsRemoved()
            {
                LinearCollection collection = { 2001, 2010, 2051, 3001 };

                collection.erase(begin(collection) + 1, begin(collection) + 2);

                thenCollectionContainsValues(collection, { 2001, 2051, 3001 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingWholeRange_ThenCollectinIsEmpty()
            {
                LinearCollection collection = { 400, 403, 404 };

                collection.erase(begin(collection), end(collection));

                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenErasingRange_ThenSizeIsUpdated()
            {
                LinearCollection collection = { 23, 10, 20, 16 };

                collection.erase(begin(collection) + 1, end(collection) - 1);

                if(collection.getSize()==2) ; else throw("Failed check equal (" "collection.getSize()" "==" "2"")");
            }

            // ConstIterator is tested via Iterator methods.
            // If Iterator methods are to be changed, then new ConstIterator tests are required.

            // My own tests:

            ++number_of_tests;
            // test_GivenEmptyCollection_WhenPreallocating_ThenCapacityIsUpdated_AndCollectionIsEmpty()
            {
                LinearCollection collection;
                collection.setNumberOfPreallocatedElements(10);
                if(collection.getCapacity()==10) ; else throw("Failed check equal (" "collection.getCapacity()" "==" "10"")");
                if(collection.isEmpty()) ; else throw("Failed check (" "collection.isEmpty()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_ThenCapacityIsGreaterOrEqualSize()
            {
                LinearCollection collection = { 11, 12, 13, 24, 25, 0 };
                if(collection.getCapacity() >= collection.getSize()) ; else throw("Failed check (" "collection.getCapacity() >= collection.getSize()" ")");
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenPreallocating_ThenCapacityIsUpdated_AndValuesContinedAreTheSame()
            {
                LinearCollection collection = { 1, 2, 33, 4, 5, 6, 0 };
                collection.setNumberOfPreallocatedElements(30);
                if(collection.getCapacity()==30 + collection.getSize()) ; else throw("Failed check equal (" "collection.getCapacity()" "==" "30 + collection.getSize()"")");
                thenCollectionContainsValues(collection, { 1, 2, 33, 4, 5, 6, 0 });
            }

            ++number_of_tests;
            // test_GivenNonEmptyCollection_WhenDeletingPreallocatedElements_ThenCapacityEqualsSize_AndValuesContinedAreTheSame()
            {
                LinearCollection collection = { 1, 22, 3, 44, 5, 7 };
                collection.setNumberOfPreallocatedElements(0);
                if(collection.getCapacity()==collection.getSize()) ; else throw("Failed check equal (" "collection.getCapacity()" "==" "collection.getSize()"")");
                thenCollectionContainsValues(collection, { 1, 22, 3, 44, 5, 7 });
            }
        } // try
        catch(const char *ex) {std::cout<<ex<<"\nlast test number: "<<number_of_tests<<"\n\n";}
        catch(const std::exception &ex) {std::cout<<"Unwanted exception: "<<ex.what()<<"\nlast test number: "<<number_of_tests<<"\n\n";}
        catch(...) {std::cout<<"Unknown exception! last test number: "<<number_of_tests<<"\n\n";}
        std::cout<<"all vector tests passed\n\n";
    } // perform_vector_test
} // namepsace

#endif // AISDI_LINEAR_SIMPLE_TEST_VECTOR_H
