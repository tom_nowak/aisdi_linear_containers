#ifndef AISDI_TEST_PERFORMANCE
#define AISDI_TEST_PERFORMANCE

#include <chrono>
#include <map>

// This class allows to measure execution time (real time and processor time)
// of arbitrarily chosen blocks of code, especially functions. The measured times
// are not exact, because part of Performance's method execution time is also counted.

// Adding new block of code to the measured set is very simple - time data is
// accessible by its name, you just need to call start() and stop() with proper
// argument in proper places, and call end() at the end of program to dispaly time report.
// Functions start/stop throw std::logic_error if they are called in a wrong way:
// stop without prvious start, two starts without stop between them.

// I have written this class long before AISDI Linear project, that is why it
// is more 'complex' than it should be, and variables are named in camelCase.
// After making some changes, I decided to add it to this project.
// This class is not a singleton, because it was meant to allow to measure many calls of
// a certain function at one time (in recursive functions or multithreaded programs),
// so different class instances can be created for each measurement, however
// this is not the case with this project (but there is no point
// in making this class a singleton just for one project).

class Performance
{
public:
    Performance() { classID = ++numberOfInstances; }
    void start(const char *functionName);
    void stop(const char *functionName);
    void end();
    ~Performance() { --numberOfInstances; }

private:
    struct TimeData
    {
        unsigned long long iterations;
        std::chrono::steady_clock::time_point timePoint;
        std::chrono::duration<unsigned long long, std::micro> timeTotal, timeMin, timeMax;
        clock_t processorPoint, processorTotal, processorMin, processorMax;
        bool running;

        TimeData();
        void print(const char *functionName) const;
    };

    unsigned classID;
    std::map<const char *, TimeData> dataMap;

    static unsigned numberOfInstances;
};


// Out of indolence - I do not know how to set up linker in main CMakeLists,
// so I am making compilation time a bit longer, including .cpp file...
#include "testPerformance.cpp"
#endif // AISDI_TEST_PERFORMANCE
