#include "testPerformance.h"
#include <cstdio>
#include <climits>
#include <ctime>

using namespace std::chrono;

unsigned Performance::numberOfInstances = 0;


// "min" set to a grat value, so that it will be later replaced by some shorter times
Performance::TimeData::TimeData()
    : iterations(1), timeTotal(0), timeMin(ULLONG_MAX), timeMax(0),
      processorTotal(0), processorMin(UINT_MAX), processorMax(0), running(1)
{
    timePoint = steady_clock::now();
    processorPoint = clock();
}


void Performance::TimeData::print(const char *functionName) const
{
    double averageTime = static_cast<double>(timeTotal.count()) / static_cast<double>(iterations);
    double pMax = 1000000*static_cast<double>(processorMax)/CLOCKS_PER_SEC;
    double pMin = 1000000*static_cast<double>(processorMin)/CLOCKS_PER_SEC;
    double pTotal = 1000000*static_cast<double>(processorTotal)/CLOCKS_PER_SEC;
    double pAverage = pTotal / static_cast<double>(iterations);
    if(running)
        printf("%s (WARNING - NOT STOPPED TIME MEASUREMENT):\n"
               "iterations: %llu\n"
               "  real time:\n"
               "    total time: %llu\n"
               "    min time: %llu\n"
               "    max time: %llu\n"
               "    average time: %g\n"
               "  processor time:\n"
               "    total time: %g\n"
               "    min time: %g\n"
               "    max time: %g\n"
               "    average time: %g\n\n",
               functionName, iterations,
               timeTotal.count(), timeMin.count(), timeMax.count(),
               averageTime, pTotal, pMin, pMax, pAverage
              );
    else
        printf("%s:\n"
               "iterations: %llu\n"
               "  real time:\n"
               "    total time: %llu\n"
               "    min time: %llu\n"
               "    max time: %llu\n"
               "    average time: %g\n"
               "  processor time:\n"
               "    total time: %g\n"
               "    min time: %g\n"
               "    max time: %g\n"
               "    average time: %g\n\n",
               functionName, iterations,
               timeTotal.count(), timeMin.count(), timeMax.count(),
               averageTime, pTotal, pMin, pMax, pAverage
              );
}


void Performance::start(const char *functionName)
{
    auto data = dataMap.find(functionName);
    if(data == dataMap.end())
    {
        TimeData newTD;
        dataMap.insert(std::pair<const char *, TimeData>(functionName, newTD));
    }
    else
    {
        if(data->second.running)
            throw(std::logic_error("attempt to start the same function without stopping"));
        data->second.running = 1;
        ++data->second.iterations;
        data->second.processorPoint = clock();
        data->second.timePoint = steady_clock::now();
    }
}


void Performance::stop(const char *functionName)
{
   auto data = dataMap.find(functionName);
   if(data == dataMap.end())
      throw(std::logic_error("attempt to stop time measurement for unknown function"));
   TimeData &td = data->second;
   if(!td.running)
      throw(std::logic_error("attempt to stop non-started time measurement"));
   td.running = 0;

   clock_t tmpProcessor = clock() - td.processorPoint;
   auto tmpTime = duration_cast<duration<unsigned long long, std::micro>>(steady_clock::now()-td.timePoint);

   if(tmpTime > td.timeMax)
      td.timeMax = tmpTime;
   if(tmpTime < td.timeMin)
      td.timeMin = tmpTime;
   td.timeTotal += tmpTime;

   if(tmpProcessor > td.processorMax)
       td.processorMax = tmpProcessor;
   if(tmpProcessor < td.processorMin)
       td.processorMin = tmpProcessor;
   td.processorTotal += tmpProcessor;
}


void Performance::end()
{
    printf("\nTime report (time in microseconds) from Performance class number %u:\n\n",
           classID);
    for(auto const &data : dataMap)
    {
        data.second.print(data.first);
    }
}
