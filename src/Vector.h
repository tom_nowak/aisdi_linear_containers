/*
    Tomasz Nowak
    nr indeksu: 277117
    piątek nieparzysty, godzina 8
*/

#ifndef AISDI_LINEAR_VECTOR_H
#define AISDI_LINEAR_VECTOR_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>

namespace aisdi
{

template <typename Type>
class Vector
{
public:
    using difference_type = std::ptrdiff_t;
    using size_type = std::size_t;
    using value_type = Type;
    using pointer = Type*;
    using reference = Type&;
    using const_pointer = const Type*;
    using const_reference = const Type&;

    class ConstIterator;
    class Iterator;
    using iterator = Iterator;
    using const_iterator = ConstIterator;

    // Give iterators access to (private) pointers:
    friend class ConstIterator;
    friend class Iterator;

    Vector()
        : data_begin(nullptr), data_end(nullptr), total_end(nullptr)
    {}

    Vector(std::initializer_list<Type> l)
    {
        const size_type size = l.size();
        data_begin = new Type[size];
        total_end = data_end = data_begin + size;
        pointer ptr = data_begin;
        for(auto const &i : l)
            *ptr++ = std::move(i);
    }

    Vector(const Vector& other)
    {
        const size_type size = other.getSize();
        data_begin = new Type[size];
        total_end = data_end = data_begin + size;
        for(pointer from=other.data_begin, to = data_begin; from<other.data_end; ++from, ++to)
            *to = std::move(*from);
    }

    Vector(Vector&& other)
        : data_begin(other.data_begin), data_end(other.data_end), total_end(other.total_end)
    {
        other.data_begin = nullptr;
        other.total_end = nullptr;
        other.data_end = nullptr;
    }

    ~Vector()
    {
        delete [] data_begin;
    }

    Vector& operator=(const Vector& other)
    {
        const size_type size = other.getSize();
        delete [] data_begin;
        data_begin = nullptr; // in case new throws - to avoid deleting once again in destructor
        data_begin = new Type[size];
        total_end = data_end = data_begin + size;
        for(pointer from=other.data_begin, to=data_begin; from<other.data_end; ++from, ++to)
            *to = std::move(*from);
        return *this;
    }

    Vector& operator=(Vector&& other)
    {
        delete [] data_begin;
        data_begin = other.data_begin;
        data_end = other.data_end;
        total_end = other.total_end;
        other.data_begin = nullptr;
        other.data_end = nullptr;
        other.total_end = nullptr;
        return *this;
    }

    bool isEmpty() const
    {
        return data_begin == data_end;
    }

    size_type getSize() const // returns number of accessible elements (not preallocated ones)
    {
        return data_end - data_begin;
    }

    void append(const Type& item)
    {
        if(data_end == total_end) // vector has to be reallocated
            setNumberOfPreallocatedElements(1 + 2*getSize()); // preallocation (+1 in case of an empty container)
        *data_end++ = item;
    }

    void prepend(const Type& item)
    {
        // Code duplication with insert for optimization - no iterator created,
        // less arguments passed, in case of reallocation no need to look for elements
        // before inserted position (because in this case insert position is vector begin).
        if(data_end == total_end) // vector has to be reallocated
        {
            const size_type size = getSize();
            const size_type new_capacity = 1 + 2*size; // preallocation (+1 in case of an empty container)
            const AutoDeletedPointer old_begin(data_begin); // type - simple data structure, defined private
            const pointer old_end = data_end;
            data_begin = new Type[new_capacity];
            data_end = data_begin + size + 1;
            total_end = data_begin + new_capacity;
            pointer old_ptr = old_begin.ptr;
            pointer new_ptr = data_begin;
            *data_begin = item; // insert new item
            while(old_ptr!=old_end) // move elements after insert_position.data
                *++new_ptr = std::move(*old_ptr++);
            // old data is deleted in old_begin's destructor
        }
        else
        {
            // move elements after insert_position.data - for one "to the right":
            for(pointer ptr=data_begin; ptr!=data_end; ++ptr)
                *(ptr+1) = std::move(*ptr);
            ++data_end;
            *data_begin = item; // insert new item
        }
    }

    void insert(const const_iterator& insert_position, const Type& item)
    {
        if(data_end == total_end) // vector has to be reallocated
        {
            const size_type size = getSize();
            const size_type new_capacity = 1 + 2*size; // preallocation (+1 in case of an empty container)
            const AutoDeletedPointer old_begin(data_begin); // type - simple data structure, defined private
            const pointer old_end = data_end;
            data_begin = new Type[new_capacity];
            data_end = data_begin + size + 1;
            total_end = data_begin + new_capacity;
            pointer old_ptr = old_begin.ptr;
            pointer new_ptr = data_begin;
            while(old_ptr!=insert_position.data) // move elements before insert_position.data
                *new_ptr++ = std::move(*old_ptr++);
            *(data_begin - old_begin.ptr + insert_position.data) = item; // insert new item
            while(old_ptr!=old_end) // move elements after insert_position.data
                *++new_ptr = std::move(*old_ptr++);
            // old data is deleted in old_begin's destructor
        }
        else
        {
            // move elements after insert_position.data - for one "to the right":
            for(pointer ptr=insert_position.data; ptr!=data_end; ++ptr)
                *(ptr+1) = std::move(*ptr);
            ++data_end;
            *insert_position.data = item; // insert new item
        }
    }

    // In functions popFirst, popLast, erase(1), erase(1,2) there could be the following
    // operation: if(getCapacity()>4*getSize()) setNumberOfPreallocatedElements(0); but I don't
    // do it, because I don't want to hurt vector's performance in case of irregular insert /
    // erase etc. I believe that the User will know when some memory is not needed, so function
    // setNumberOfPreallocatedElements (look below) is public - but it should be used wisely.

    Type popFirst()
    {
        // Code duplication with void erase(const const_iterator &position) for optimization -
        // no iterator created, no argument passed, simpler validation at the beginning.
        if(isEmpty())
            throw std::logic_error("attempt to popFirst in an empty container");
        Type tmp = *data_begin;
        pointer to_overwrite = data_begin;
        pointer to_move = data_begin+1;
        // Move all elements after first position for one place to the left
        while(to_move!=data_end)
            *to_overwrite++ = std::move(*to_move++);
        // Do not delete unwanted elements - only change pointer data_end:
        data_end = to_overwrite;
        return tmp;
    }

    Type popLast()
    {
        if(isEmpty())
            throw std::logic_error("attempt to popLast in an empty container");
        return *--data_end;
    }

    void erase(const const_iterator &position)
    {
        // Code duplication with 2-argument erase for optimization -
        // no additionl argument passed, simpler validation at the beginning.
        if(iteratorCannotBeDereferenced(position))
            throw(std::out_of_range("attmept to erase element out of vector's bounds"));
        pointer to_overwrite = position.data;
        pointer to_move = to_overwrite+1;
        // Move all elements after given position for one place to the left
        while(to_move!=data_end)
            *to_overwrite++ = std::move(*to_move++);
        // Do not delete unwanted elements - only change pointer data_end:
        data_end = to_overwrite;
    }

    void erase(const const_iterator& firstIncluded, const const_iterator& lastExcluded)
    {
        if(iteratorCannotBeDereferenced(firstIncluded) || iteratorIsOutOfRange(lastExcluded))
            throw(std::out_of_range("attmept to erase range out of vector's bounds"));
        if(firstIncluded.data > lastExcluded.data)
            throw(std::logic_error("first after last vector's range to erase"));
        pointer to_overwrite = firstIncluded.data;
        pointer to_move = lastExcluded.data;
        // Move elements from the back to new free places (after firstIncluded.data):
        while(to_move!=data_end)
            *to_overwrite++ = std::move(*to_move++);
        // Do not delete unwanted elements - only change pointer data_end:
        data_end = to_overwrite;
    }

    iterator begin()
    {
        // using constructor: Iterator(const ConstIterator& other)
        return iterator(cbegin());
    }

    iterator end()
    {
        return iterator(cend());
    }

    const_iterator cbegin() const
    {
        return const_iterator(data_begin, this);
    }

    const_iterator cend() const
    {
        return const_iterator(data_end, this);
    }

    const_iterator begin() const
    {
        return cbegin();
    }

    const_iterator end() const
    {
        return cend();
    }

    // Public function added by me: getCapacity -
    // returns number of all elements, including preallocated ones
    size_type getCapacity() const
    {
        return total_end - data_begin;
    }

    // Public function added by me: setNumberOfPreallocatedElements -
    // changes number of preallocated (unused) elements according to its argument.
    // In particular, for number_of_elements==0, all preallocated elements are deleted.
    // This function should be used wisely, because calling it without reason
    // makes vector's performance worse due to often memory reallocations.
    void setNumberOfPreallocatedElements(const size_type number_of_elements)
    {
        const size_type size = getSize(); // need to save it before data_begin is changed
        const AutoDeletedPointer old_begin(data_begin);
        const pointer old_end = data_end;
        data_begin = new Type[number_of_elements + size];
        data_end = data_begin + size;
        total_end = data_end + number_of_elements;
        pointer ptr_new = data_begin;
        pointer ptr_old = old_begin.ptr;
        while(ptr_old != old_end)
            *ptr_new++ = std::move(*ptr_old++);
        // old data is deleted in old_begin's destructor
    }

private:
    // Data is allocated in continuous space with 3 marking pointers:
    pointer data_begin, data_end, total_end;
    // [data_begin, data_end) - range from user's point of view - used, accesible elements
    // [data_begin, total_end) - the whole range - all allocated elements
    // [data_end, total_end) - preallocated - not used, but ready to be filled up

    // Simple data structure to make vector reallocation (situation in which for a short time
    // there are 2 blocks of allocated memory at once) exception-safe (to avoid memory leaks):
    struct AutoDeletedPointer
    {
        const pointer ptr; // ptr must be a pointer to data allocated with new []
        explicit AutoDeletedPointer(const pointer p) : ptr(p) {}
        ~AutoDeletedPointer() { delete [] ptr; }
    };
    // Note: data_begin is a normal pointer, not auto deleted one, because there is vector's destructor
    // anyway and it it easy to write 'delete [] data_begin', moreover data_begin is not const.
    // AutoDeletedPointer is used only for local variables storing temporary pointers to allocated memory.

    // Functions below would be public if there were no exceptions in iterator's operators * ++ --
    // Iterator correctness is controlled anyway, so there is no need to make
    // these functions public - calling them by the user would be redundant.
    // These functions are Vector's, not ConstIterator's methods, because otherwise it would be possible
    // to (for example) call erase with argument - iterator from another vector and iterator's
    // correctness would be checked according to vector's pointer in iterator, not the vector
    // for which function erase was called, which might cause problems.
    bool iteratorIsOutOfRange(const const_iterator &it) const
    {
        return it.data < data_begin || it.data > data_end;
    }

    bool iteratorCannotBeDereferenced(const const_iterator &it) const // the same as above + end included
    {
        return it.data < data_begin || it.data >= data_end;
    }
}; // class Vector

template <typename Type>
class Vector<Type>::ConstIterator
{
public:
    friend class Vector<Type>; // to enable iterator construction out of raw pointer

    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = typename Vector::value_type;
    using difference_type = typename Vector::difference_type;
    using pointer = typename Vector::const_pointer;
    using reference = typename Vector::const_reference;

    explicit ConstIterator()
    {}

    // Throws std::out_of_range if pointed data is not within vector's rangee
    reference operator*() const
    {
        if(vector->iteratorCannotBeDereferenced(*this))
            throw(std::out_of_range("attempt to dereference iterator out of vector's range"));
        return *data;
    }

    // Throws std::out_of_range if pointed data is not within vector's rangee
    ConstIterator& operator++()
    {
        ++data;
        // checking [begin, end], not just end, because pointer might be invalid after reallocation
        if(vector->iteratorIsOutOfRange(*this))
            throw(std::out_of_range("operator++() - out of vector's range"));
        return *this;
    }

    // Throws std::out_of_range if pointed data is not within vector's rangee
    ConstIterator operator++(int)
    {
        ConstIterator tmp = *this;
        ++data;
        if(vector->iteratorIsOutOfRange(*this))
            throw(std::out_of_range("operator++(int) - out of vector's range"));
        return tmp;
    }

    // Throws std::out_of_range if pointed data is not within vector's rangee
    ConstIterator& operator--()
    {
        --data;
        if(vector->iteratorIsOutOfRange(*this))
            throw(std::out_of_range("operator--() - out of vector's range"));
        return *this;
    }

    // Throws std::out_of_range if pointed data is not within vector's rangee
    ConstIterator operator--(int)
    {
        ConstIterator tmp = *this;
        --data;
        if(vector->iteratorIsOutOfRange(*this))
            throw(std::out_of_range("operator--(int) - out of vector's range"));
        return tmp;
    }

    // Warning: in operator +/- vector's range is not checked for
    // performance reasons (however nothing bad happens - only the pointer is
    // changed and there will be bounds checking anyway in case of dereference).
    ConstIterator operator+(difference_type d) const
    {
        // Works correctly even if d<0, unlike in lists, where sign of d should be checked
        return ConstIterator(data + d, vector);
    }

    ConstIterator operator-(difference_type d) const
    {
        return ConstIterator(data - d, vector);
    }

    // Safe - does not throw exception or cause access violation
    bool operator==(const ConstIterator& other) const
    {
        // Different vector instances do not share data, so I do not check
        // vector pointers. In the case when data is not in range
        // of any vector, this comparison would not make sense anyway, but it is not
        // checked - this function returns something even for incorrect iterators.
        return data == other.data;
    }

    // Safe - does not throw exception or cause access violation
    bool operator!=(const ConstIterator& other) const
    {
        return data != other.data;
    }

private:
    Type *data;

    // Range in operators: * ++ -- needs to be controlled, so pointer to the associated
    // container is stored. In case of exceeding vector's range, std::out_of_range is thrown.
    const Vector *vector;

    // Private constructor called by friend class Vector
    ConstIterator(Type *d, const Vector *associated_container)
        : data(d), vector(associated_container)
    {}
}; // class Vector<Type>::ConstIterator

template <typename Type>
class Vector<Type>::Iterator : public Vector<Type>::ConstIterator
{
public:
    using pointer = typename Vector::pointer;
    using reference = typename Vector::reference;

    explicit Iterator()
    {}

    Iterator(const ConstIterator& other)
        : ConstIterator(other)
    {}

    Iterator& operator++()
    {
        ConstIterator::operator++();
        return *this;
    }

    Iterator operator++(int)
    {
        auto result = *this;
        ConstIterator::operator++();
        return result;
    }

    Iterator& operator--()
    {
        ConstIterator::operator--();
        return *this;
    }

    Iterator operator--(int)
    {
        auto result = *this;
        ConstIterator::operator--();
        return result;
    }

    Iterator operator+(difference_type d) const
    {
        return ConstIterator::operator+(d);
    }

    Iterator operator-(difference_type d) const
    {
        return ConstIterator::operator-(d);
    }

    reference operator*() const
    {
        // ugly cast, yet reduces code duplication.
        return const_cast<reference>(ConstIterator::operator*());
    }
}; // class Vector<Type>::Iterator : public Vector<Type>::ConstIterator

}

#endif // AISDI_LINEAR_VECTOR_H
