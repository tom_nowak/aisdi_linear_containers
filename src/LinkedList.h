/*
    Tomasz Nowak
    nr indeksu: 277117
    piątek nieparzysty, godzina 8
*/

#ifndef AISDI_LINEAR_LINKEDLIST_H
#define AISDI_LINEAR_LINKEDLIST_H

#include <cstddef>
#include <initializer_list>
#include <stdexcept>

namespace aisdi
{

template <typename Type>
class LinkedList
{
public:
    using difference_type = std::ptrdiff_t;
    using size_type = std::size_t;
    using value_type = Type;
    using pointer = Type*;
    using reference = Type&;
    using const_pointer = const Type*;
    using const_reference = const Type&;

    class ConstIterator;
    class Iterator;
    using iterator = Iterator;
    using const_iterator = ConstIterator;

    // Give iterators access to (private) struct Node:
    friend class ConstIterator;
    friend class Iterator;

    LinkedList()
    {
        constructSentinel();
    }

    LinkedList(std::initializer_list<Type> l)
    {
        constructSentinel();
        for(auto const &item : l)
            append(std::move(item));
    }

    LinkedList(const LinkedList& other)
    {
        constructSentinel();
        for(auto const &item : other)
            append(item);
    }

    LinkedList(LinkedList&& other)
        : sentinel(other.sentinel)
    {
        // 'other' must be left in a valid state - in this case this is an empty list
        other.constructSentinel();
    }

    ~LinkedList()
    {
        // fast clear, as if it were one-directional list
        Node *tmp;
        sentinel->prev->next = nullptr;
        while(sentinel)
        {
            tmp = sentinel;
            sentinel = sentinel->next;
            delete tmp;
        }
    }

    LinkedList& operator=(const LinkedList& other)
    {
        clear();
        for(auto const &item : other)
            append(item);
        return *this;
    }

    LinkedList& operator=(LinkedList&& other)
    {
        // erase all the elements - to meet the requirements of test:
        // 'GivenNonEmptyCollection_WhenMoveAssigning_ThenAllElementsAreMoved'
        clear();
        std::swap(sentinel, other.sentinel); // now 'other' will be empty
        return *this;
    }

    bool isEmpty() const
    {
        return sentinel->next == sentinel;
    }

    size_type getSize() const
    {
        size_type retval = 0;
        // pointers insead of iterators to avoid redundant bounds checking
        for(Node *it = sentinel->next; it!=sentinel; it=it->next)
            ++retval;
        return retval;
    }


    // Some code duplication for optimization (very small, but that is always something) -
    // there are different functions for adding element: at any position given by iterator and
    // 2 special cases: append/prepend (direct access to sentinel without creating iterator).
    // Each of this functions has 2 versions, allowing to copy or move added data.

    void append(const Type& item)
    {
        setUpPointersForNewElement(new Node(item), sentinel);
    }

    void append(Type &&item)
    {
        setUpPointersForNewElement(new Node(std::move(item)), sentinel);
    }

    void prepend(const Type& item)
    {
        setUpPointersForNewElement(new Node(item), sentinel->next);
    }

    void prepend(Type &&item)
    {
        setUpPointersForNewElement(new Node(std::move(item)), sentinel->next);
    }

    void insert(const const_iterator& insertPosition, const Type& item)
    {
        setUpPointersForNewElement(new Node(item), insertPosition.node);
    }

    void insert(const const_iterator& insertPosition, Type &&item)
    {
        setUpPointersForNewElement(new Node(std::move(item)), insertPosition.node);
    }

    Type popFirst()
    {
        if(isEmpty())
            throw std::logic_error("attempt to popFirst in an empty container");
        auto tmp = begin();
        Type retval = tmp.node->data;
        erase(tmp);
        return retval;
    }

    Type popLast()
    {
        if(isEmpty())
            throw std::logic_error("attempt to popLast in an empty container");
        auto tmp = end()-1;
        Type retval = tmp.node->data;
        erase(tmp);
        return retval;
    }

    // Warning - using iterator after function erase causes undefined behavior!
    void erase(const const_iterator& position)
    {
        Node *n = position.node;
        if(n==sentinel)
            throw(std::out_of_range("attmept to erase LinkedList::end()"));
        n->prev->next = n->next;
        n->next->prev = n->prev;
        // To prevent undefined behaviour, I could do something like: position.node = n->next;
        // or return const_iterator(n->next, this) or set some flag inside iterator,
        // but I don't want to change this funcion prototype (void, const argument)
        // and even if I did it, it would not prevent all the errors - for example having
        // a few iterators pointing to the erased node would bypass all the security measures.
        delete n;
    }

    void erase(const const_iterator& firstIncluded, const const_iterator& lastExcluded)
    {
        Node *firstExcluded = firstIncluded.node->prev;
        Node *to_delete;
        // Delete data in loop:
        for(Node *n = firstIncluded.node; n!=lastExcluded.node; )
        {
            if(n==sentinel) // happens when firstIncluded is closer to end than lastExcluded
            {
                // leave list in a valid state in case of an error - the range of deleted
                // elements is the same as if function was called with list end as 'lastExcluded'
                firstExcluded->next = sentinel;
                sentinel->prev = firstExcluded;
                throw(std::out_of_range("attmept to erase range including LinkedList::end()"));
            }
            to_delete = n;
            n = n->next;
            delete to_delete;
        }
        firstExcluded->next = lastExcluded.node;
        lastExcluded.node->prev = firstExcluded;
    }

    iterator begin()
    {
        return iterator(cbegin());
    }

    iterator end()
    {
        return iterator(cend());
    }

    const_iterator cbegin() const
    {
        return const_iterator(sentinel->next, this);
    }

    const_iterator cend() const
    {
        return const_iterator(sentinel, this);
    }

    const_iterator begin() const
    {
        return cbegin();
    }

    const_iterator end() const
    {
        return cend();
    }

private:
    struct Node
    {
        Type data;
        Node *next, *prev;

        Node() {}
        Node(const Type &d) : data(d) {}
        Node(Type &&d) : data(std::move(d)) {}
    };

    // Non-accessible element at the beginning of the list - makes
    // operations on the list easier - the same functions can be used
    // for accessing all the elements without additional if statements.
    // From implementation point of view, this list is in fact a
    // bidirectional ring with one special element - sentinel.
    Node *sentinel;

    // In case of an empty list, the sentinel points to itself (in both directions).
    void constructSentinel()
    {
        sentinel = new Node;
        sentinel->next = sentinel->prev = sentinel;
    }

    void setUpPointersForNewElement(Node *new_node, Node *position)
    {
        new_node->next = position;
        new_node->prev = position->prev;
        position->prev->next = new_node;
        position->prev = new_node;
    }

    void clear()
    {
        Node *deleted, *tmp = sentinel->next;
        while(tmp!=sentinel)
        {
            deleted = tmp;
            tmp = tmp->next;
            delete deleted;
        }
        sentinel->next = sentinel->prev = sentinel;
    }
};

template <typename Type>
class LinkedList<Type>::ConstIterator
{
public:
    using iterator_category = std::bidirectional_iterator_tag;
    using value_type = typename LinkedList::value_type;
    using difference_type = typename LinkedList::difference_type;
    using pointer = typename LinkedList::const_pointer;
    using reference = typename LinkedList::const_reference;

    friend class LinkedList<Type>;

    explicit ConstIterator()
    {}

    reference operator*() const
    {
        if(node == list->sentinel)
            throw std::out_of_range("dereference LinkedList::cend() iterator");
        return node->data;
    }

    ConstIterator& operator++()
    {
        if(node == list->sentinel)
            throw(std::out_of_range("exceeding list end in operator++()"));
        node = node->next;
        return *this;
    }

    ConstIterator operator++(int)
    {
        if(node == list->sentinel)
            throw(std::out_of_range("exceeding list end in operator++(int)"));
        ConstIterator tmp = *this;
        node = node->next;
        return tmp;
    }

    ConstIterator& operator--()
    {
        if(node->prev == list->sentinel)
            throw(std::out_of_range("exceeding list begin in operator--()"));
        node = node->prev;
        return *this;
    }

    ConstIterator operator--(int)
    {
        if(node->prev == list->sentinel)
            throw(std::out_of_range("exceeding list begin in operator--(int)"));
        ConstIterator tmp = *this;
        node = node->prev;
        return tmp;
    }

    ConstIterator operator+(difference_type d) const
    {
        if(d<0)
            return operator-(-d);
        auto tmp = *this;
        while(d--)
        {
            if(node == list->sentinel)
                throw(std::out_of_range("exceeding list end in operator+"));
            tmp.node = tmp.node->next;
        }
        return tmp;
    }

    ConstIterator operator-(difference_type d) const
    {
        if(d<0)
            return operator+(-d);
        auto tmp = *this;
        while(d--)
        {
            if(tmp.node->prev == list->sentinel)
                throw(std::out_of_range("exceeding list begin in operator-"));
            tmp.node = tmp.node->prev;
        }
        return tmp;
    }

    bool operator==(const ConstIterator& other) const
    {
        // no need to check list pointer, because different list instances
        // do not share data (given node can be only in one list)
        return node == other.node;
    }

    bool operator!=(const ConstIterator& other) const
    {
        return node != other.node;
    }

private:
    LinkedList::Node *node;
    const LinkedList *list;

    ConstIterator(LinkedList::Node *n, const LinkedList *associatedContainer)
        : node(n), list(associatedContainer)
    {}
};

template <typename Type>
class LinkedList<Type>::Iterator : public LinkedList<Type>::ConstIterator
{
public:
    using pointer = typename LinkedList::pointer;
    using reference = typename LinkedList::reference;

    explicit Iterator()
    {}

    Iterator(const ConstIterator& other)
        : ConstIterator(other)
    {}

    Iterator& operator++()
    {
        ConstIterator::operator++();
        return *this;
    }

    Iterator operator++(int)
    {
        auto result = *this;
        ConstIterator::operator++();
        return result;
    }

    Iterator& operator--()
    {
        ConstIterator::operator--();
        return *this;
    }

    Iterator operator--(int)
    {
        auto result = *this;
        ConstIterator::operator--();
        return result;
    }

    Iterator operator+(difference_type d) const
    {
        return ConstIterator::operator+(d);
    }

    Iterator operator-(difference_type d) const
    {
        return ConstIterator::operator-(d);
    }

    reference operator*() const
    {
        // ugly cast, yet reduces code duplication.
        return const_cast<reference>(ConstIterator::operator*());
    }
};

}

#endif // AISDI_LINEAR_LINKEDLIST_H
